WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CONNECT &1/&2

CREATE OR REPLACE FUNCTION mqs_ar_valor_recebido_f(p_payment_schedule_id NUMBER, p_dt_ref DATE) RETURN NUMBER IS
-- $Header: mqs_ar_valor_recebido_f.pls 120.1 2019/06/24  12:45:00 appldev ship                                       $
-- +=================================================================+
-- |            Copyright (c) 2010 Grupo Edson Queiroz               |
-- |            Fortaleza, Brasil, All rights reserved.              |
-- +=================================================================+
-- | FILENAME                                                        |
-- |   mqs_ar_valor_recebido_f.pls                                   |
-- |                                                                 |
-- | PURPOSE                                                         |
-- |   Esta fun��o retorno o valor pago de uma fatura conforme a     |
-- |   data de referencia                                            |
-- |                                                                 |
-- | DESCRIPTION                                                     |
-- |   Esta fun��o retorno o valor pago de uma fatura conforme a     |
-- |   data de referencia                                            |
-- |                                                                 |
-- | CREATED BY                                                      |
-- |   Ant�nio M�rio                                 24/06/2019      |
-- +=================================================================+
--
  ln_valor_rec                    NUMBER;
BEGIN
  SELECT sum(nvl(ara.amount_applied, 0) - nvl(ad.amount, 0))
    INTO ln_valor_rec
    FROM ra_customer_trx_all              rcta
         , ar_payment_schedules_all       apsa left join ar_adjustments_all ad on ad.payment_schedule_id = apsa.payment_schedule_id
                                                                                                AND ad.status = 'A'
         , ra_cust_trx_types_all          rctt
         , ar_cash_receipts_all           acra
         , ar_receipt_methods             arm
         , ar_receivable_applications_all ara
   WHERE rcta.customer_trx_id = apsa.customer_trx_id
     AND rctt.cust_trx_type_id = rcta.cust_trx_type_id
     AND rctt.attribute_category = 'Informacoes Centro Fashion'
     --AND rctt.attribute1 IN ('CDU', 'MALL E MIDIA')
     AND rcta.org_id = apsa.org_id

     AND apsa.org_id = acra.org_id
     AND apsa.customer_id = acra.pay_from_customer
     AND apsa.customer_site_use_id = acra.customer_site_use_id

     AND acra.receipt_method_id = arm.receipt_method_id
     AND ara.cash_receipt_id = acra.cash_receipt_id
     AND ara.applied_customer_trx_id = rcta.customer_trx_id
     AND ara.status = 'APP'

     AND apsa.org_id = ara.org_id
     AND apsa.payment_schedule_id = ara.applied_payment_schedule_id
     AND apsa.payment_schedule_id = p_payment_schedule_id
     AND TRUNC(ara.apply_date) <= p_dt_ref;

  RETURN nvl(ln_valor_rec, 0);
END mqs_ar_valor_recebido_f;

/
EXIT
