WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CONNECT &1/&2

CREATE OR REPLACE FUNCTION mqs_ar_qtd_parcelas_f(p_customer_trx_id NUMBER) RETURN NUMBER IS
-- $Header: mqs_ar_qtd_parcelas_f.pls 120.1 2019/06/24 12:45:00 appldev ship                                       $
-- +=================================================================+
-- |            Copyright (c) 2010 Grupo Edson Queiroz               |
-- |            Fortaleza, Brasil, All rights reserved.              |
-- +=================================================================+
-- | FILENAME                                                        |
-- |   mqs_ar_qtd_parcelas_f.pls                                     |
-- |                                                                 |
-- | PURPOSE                                                         |
-- |   Esta fun��o retorno a quantidade de parcelas da fatura        |
-- |                                                                 |
-- | DESCRIPTION                                                     |
-- |   Esta fun��o retorno a quantidade de parcelas da fatura        |
-- |                                                                 |
-- | CREATED BY                                                      |
-- |   Ant�nio M�rio                                 24/06/2019      |
-- +=================================================================+
--
  ln_qtd_parcela                    NUMBER;
BEGIN
  SELECT max(terms_sequence_number)
    INTO ln_qtd_parcela
    FROM ar_payment_schedules_all       apsa
   WHERE apsa.customer_trx_id = p_customer_trx_id;

  RETURN ln_qtd_parcela;
END mqs_ar_qtd_parcelas_f;

/
EXIT
